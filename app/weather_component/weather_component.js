export default {
    data: () => {
        return {
            humidity: undefined,
            temp: undefined,
            city: "",
        };
    },
    mounted() {
    },
    methods: {
        async getWeatherByCity () {
            console.log(this.city)
            fetch(`http://api.openweathermap.org/data/2.5/weather?appid=dc884d8347e8b27fc4bbbc265f2e9d3c&q=${this.city}`).then(response => response.json()).then(result => {
                this.temp = result.main.temp;
                this.humidity = result.main.humidity
            });
        }
    },
}
