import Vue from 'vue';
import WeatherComponent from './weather_component/weather_component.vue'


// =================================================== //

// Your code goes here.
new Vue({
    render: h => h(WeatherComponent)
}).$mount('#mount')